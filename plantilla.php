<!DOCTYPE HTML>
<html lang = "en">
<head>
  <title>Form</title>
  <link rel="stylesheet" href="css.css">
  <script
    src="https://code.jquery.com/jquery-3.1.1.js"
    integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="
    crossorigin="anonymous"></script>
  <script src="js.js"></script>
</head>
<body>

<?php
// define variables and set to empty values
$nameErr = $emailErr = $genderErr = $websiteErr = "";
$name = $email = $gender = $comment = $studies = "";
$allOK = false;

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

function checkEmail($emailaddress){
  $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';

  if (preg_match($pattern, $emailaddress) === 1)
    return true;
  else
    return false;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {

  $allOK = true;
  if (empty($_POST["name"])) {
    $nameErr = "Name is required";
    $allOK = false;
  } else {
    $name = test_input($_POST["name"]);
  }

  if (empty($_POST["email"]) || !checkEmail($_POST['email'])) {
    $emailErr = "Email is required";
    $allOK = false;
  } else {
    $email = test_input($_POST["email"]);
  }

  if (empty($_POST["studies"])) {
    $studies = "";
    $allOK = false;
  } else {
    $studies = test_input($_POST["studies"]);
  }

  if (empty($_POST["comment"])) {
    $comment = "";
  } else {
    $comment = test_input($_POST["comment"]);
  }

  if (empty($_POST["gender"])) {
    $genderErr = "Gender is required";
    $allOK = false;
  } else {
    $gender = test_input($_POST["gender"]);
  }

}

if(!$allOK){
// Si hi ha cap error en el formulari, el mostrem amb les dades inserides per a que sigui revisat per l'usuari
?>

  <h1>PHP Form Validation Example</h1>
  <form id = "formid" name = "test_form" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
    <p><span class="error">* required field.</span></p>
    <label for = "name">Name:</label>
    <input id = "name" type="text" name="name" value = "<?php echo $name; ?>" required>
    <span class="error">* <?php echo $nameErr;?></span>
    <br><br>
    <label for = "email">E-mail:</label>
    <input id = "email" type="email" name="email" value = "<?php echo $email; ?>" required>
    <span class="error">* <?php echo $emailErr;?></span>
    <br><br>
    <label for = "studies">Studies:</label>
    <select id = "studies" name = "studies" value = "<?php echo $studies; ?>" required>
      <option value = "ESO" selected>ESO</option>
      <option value = "Batxillerat">Batxillerat</option>
      <option value = "SMX">SMX</option>
      <option value = "DAM">DAM</option>
      <option value = "DAW">DAW</option>
      <option value = "ASIX">ASIX</option>
      <option value = "Other">Other</option>
    </select>
    <span class="error"><?php echo $websiteErr;?></span>
    <br><br>
    <label for = "comment">Comment:</label>
    <textarea id = "comment" name="comment" rows="5" cols="40" ></textarea>
    <br><br>
    <fieldset>
      <legend>Gender</legend>
      <span class="error">* <?php echo $genderErr;?></span> <br>

      <input id = "female" type="radio" name="gender" value="female" checked="checked">
      <label for = "female">Female</label> <br>

      <input id = "male" type="radio" name="gender" value="male">
      <label for = "male">Male</label>
    </fieldset>
    <br><br>
    <input id = "submit" name = "submit" type = "submit" value = "submit" >
  </form>

<?php
// Si les dades del formulari són correctes, només mostrarem un resum de les mateixes, sense el formulari
} else {
  echo "<h2>Your Input:</h2><div>Contingut extra per a que no salti error d'accesibilitat [Header sense contingut]</div>";
  echo "Name: " . $name . "<br>";
  echo "Email: " . $email . "<br>";
  echo "Studies: " . $studies . "<br>";
  echo "Comment: " . $comment . "<br>";
  echo "Gender: " . $gender;
}
?>

</body>
</html>
