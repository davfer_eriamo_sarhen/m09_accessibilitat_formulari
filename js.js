$(function(){
var $form,
allOk,
name,
mail,
studies,
comment,
url;

$("#submitBtn").click(function (event){

  event.preventDefault();

  // Reset Globals Variabes
  $form = $("#formid");
  allOk = true;
  fieldsToCheckList = "";
  name = $form.find( "input[name='name']" ).val();
  mail = $form.find( "input[name='email']" ).val();
  studies = $form.find( "input[name='studies']" ).val();
  url = $form.attr( "action" );

  checkFields();

  if (allOk){
    console.log("TRUE");
    return true;
  } else {
    console.log("FALSE");
    return false;
  }

});

function checkFields(){
  // Check if any field are empty or email is invalid
  if (name == "") {
    allOk = false;
    $form.find( "input[name='name']" ).attr("placeholder", "Name is required*");
  }
  if (email == "" || !validateEmail(email)){
    allOk = false;
    $form.find( "input[name='email']" ).attr("placeholder", "Email is required*");
  }
  if (studies == "") {
    allOk = false;
    $form.find( "input[name='studies']" ).attr("placeholder", "Subject is required*");
  }

  function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
}

});
